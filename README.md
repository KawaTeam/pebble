Pebble
======

Django based website to manage little community.

Dependencies
------------

* python3 >= 3.6
* python3-django >= 1.11.15
* python3-magic >= 0.4.15
* python3-markdown >= 2.6.11
* python3-pillow >= 5.3.0
* python3-pygments >= 2.2.0
