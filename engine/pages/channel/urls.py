# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf.urls import url
from django.conf.urls import include

# Pebble
from engine.pages.channel.views import TopicView
from engine.pages.channel.views import CommentView
from engine.pages.channel.views import ChannelView

# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [

    # ----------------------------------------
    #   Actions
    # ----------------------------------------

    url(r'^action/', include([

        # ----------------------------------------
        #   Channels
        # ----------------------------------------

        url(r'^channel/', include([
            url(r'^add/$',
                ChannelView.add, name="action_channel_add"),
            url(r'^edit/(?P<channel_id>[\d]+)/$',
                ChannelView.edit, name="action_channel_edit"),
            url(r'^delete/(?P<channel_id>[\d]+)/$',
                ChannelView.delete, name="action_channel_delete"),
            url(r'^page/(?P<channel_id>[\d]+)/$',
                ChannelView.page, name="action_channel_page")
        ])),

        # ----------------------------------------
        #   Topics
        # ----------------------------------------

        url(r'^topic/', include([
            url(r'^add/(?P<channel_id>[\d]+)/$',
                TopicView.add, name="action_topic_add"),
            url(r'^edit/(?P<topic_id>[\d]+)/$',
                TopicView.edit, name="action_topic_edit"),
            url(r'^delete/(?P<topic_id>[\d]+)/$',
                TopicView.delete, name="action_topic_delete"),
            url(r'^page/(?P<channel_id>[\d]+)/(?P<topic_id>[\d]+)/$',
                TopicView.page, name="action_topic_page" )
        ])),

        # ----------------------------------------
        #   Comments
        # ----------------------------------------

        url(r'^comment/', include([
            url(r'^add/(?P<topic_id>[\d]+)/$',
                CommentView.add, name="action_comment_add"),
            url(r'^edit/(?P<comment_id>[\d]+)/$',
                CommentView.edit, name="action_comment_edit"),
            url(r'^delete/(?P<comment_id>[\d]+)/$',
                CommentView.delete, name="action_comment_delete"),
            url(r'^switch/(?P<comment_id>[\d]+)/$',
                CommentView.switch, name="action_comment_switch")
        ]))
    ])),

    # ----------------------------------------
    #   Channels
    # ----------------------------------------

    url(r'^(?P<identifier>[^\s\/]+)-(?P<index>[^\s\/]+)/', include([
        url(r'^$',
            ChannelView.index, name="channel_look"),
        url(r'^(?P<page>\d+)/$',
            ChannelView.index, name="channel_page"),
    ])),

    # ----------------------------------------
    #   Topics
    # ----------------------------------------

    url(r'^topic/(?P<identifier>[^\s\/]+)-(?P<index>[^\s\/]+)/', include([
        url(r'^$',
            TopicView.index, name="topic_look"),
        url(r'^(?P<page>[\d]+)/$',
            TopicView.index, name="topic_page")
    ]))
]
