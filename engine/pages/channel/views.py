# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django import forms

from django.conf import settings

from django.urls import reverse

from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.shortcuts import get_object_or_404

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from django.core.exceptions import ObjectDoesNotExist

from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required

# Pebble
from engine.common import generate_identifier

from engine.pages.channel.models import Topic
from engine.pages.channel.models import Channel
from engine.pages.channel.models import Comment

# Regex
from re import sub as re_sub

# ------------------------------------------------------------------------------
#   Forms
# ------------------------------------------------------------------------------

class ChannelForm(forms.ModelForm):

    class Meta:

        model = Channel

        fields = [
            "title",
            "description",
            "category"
        ]


class TopicForm(forms.ModelForm):

    class Meta:

        model = Topic

        fields = [
            "title",
            "content",
            "lock",
            "stick"
        ]


class CommentForm(forms.ModelForm):

    class Meta:

        model = Comment

        fields = [
            "content"
        ]

        widgets = {
            "content": forms.Textarea(attrs={
                "placeholder": _("Write your comment here…") }),
        }

# ------------------------------------------------------------------------------
#   Views
# ------------------------------------------------------------------------------

class ChannelView(object):

    @login_required()
    def index(request, identifier, index, page=None):
        """ Show channel view

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        identifier : str
            Channel identifier
        index : int
            Channel index
        page : int, optional
            Paginator current page
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        channel = get_object_or_404(Channel, pk=index)

        paginator = Paginator(Topic.objects.filter(
            channel=channel).order_by("-stick", "-updated"),
            settings.PAGES["topic"])

        # Load topics for specified page
        try:
            topics = paginator.page(page)

        except PageNotAnInteger as error:
            topics = paginator.page(1)

        except EmptyPage as error:
            topics = paginator.page(paginator.num_pages)

        # ----------------------------------------
        #   Comments
        # ----------------------------------------

        for topic in topics:

            # Append topic's comment to topic instance
            topic.comments = Comment.objects.filter(topic=topic)

            if len(topic.comments) > 0:
                topic.last_message = topic.comments.latest("created")

            else:
                topic.last_message = None

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/channel/index.html", {
            "page": channel.pk,
            "user": login,
            "topics": topics,
            "channel": channel
        })


    @login_required()
    def page(request, channel_id):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : int
            Channel primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        channel = get_object_or_404(Channel, pk=channel_id)

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(reverse("channel_page",
            args=( channel.identifier, channel.pk, page )))


    @login_required()
    def add(request):
        """ Add a new channel

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.add_channel"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        if request.method == "POST":
            form = ChannelForm(request.POST)

            if form.is_valid():

                try:
                    pk = str(Channel.objects.latest("pk").pk + 1)

                except ObjectDoesNotExist as error:
                    pk = '1'

                # Generate channel identifier
                form.cleaned_data["identifier"] = generate_identifier(
                    form.cleaned_data["title"])

                channel = Channel(**form.cleaned_data)
                channel.save()

                return HttpResponseRedirect(reverse("channel_look",
                    args=( channel.identifier, channel.pk )))

        else:
            form = ChannelForm()

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/channel/edit.html", {
            "user": login,
            "form": form
        })


    @login_required()
    def edit(request, channel_id):
        """ Edit an existing channel

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : int
            Channel primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.change_channel"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        channel = get_object_or_404(Channel, pk=channel_id)

        if not login.is_staff:
            return HttpResponseRedirect(reverse("error"))

        if request.method == "POST":
            form = ChannelForm(request.POST)

            if form.is_valid():
                pk = str(channel.pk)

                for key, value in form.cleaned_data.items():
                    setattr(channel, key, value)

                # Generate channel identifier
                channel.identifier = generate_identifier(channel.title)

                channel.save()

                return HttpResponseRedirect(reverse("channel_look",
                    args=( channel.identifier, channel.pk )))

        else:
            form = ChannelForm(instance=channel)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/channel/edit.html", {
            "page": channel.pk,
            "user": login,
            "form": form,
            "channel": channel
        })


    @login_required()
    def delete(request, channel_id):
        """ Delete an existing channel

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : int
            Channel primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.delete_channel"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        channel = get_object_or_404(Channel, pk=channel_id)

        if request.method == "POST":

            # Remove channel and all his data
            if request.POST["choice"] == "yes":
                channel.delete()

                return HttpResponseRedirect(reverse("root"))

            return HttpResponseRedirect(reverse("channel_look",
                args=( channel.identifier, channel.pk )))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/channel/delete.html", {
            "page": channel.pk,
            "user": login,
            "channel": channel
        })


class TopicView(object):

    @login_required()
    def index(request, identifier, index, page=None):
        """ Show topic view

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : str
            Channel identifier
        topic_id : str
            Topic identifier
        page : int, optional
            Paginator current page
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        topic = get_object_or_404(Topic, pk=index)

        channel = get_object_or_404(Channel, pk=topic.channel.pk)

        # Append topic's comment to topic instance
        paginator = Paginator(
            Comment.objects.filter(topic=topic).order_by("created"),
            settings.PAGES["comment"])

        # Load topics for specified page
        try:
            topic.comments = paginator.page(page)

        except PageNotAnInteger as error:
            topic.comments = paginator.page(paginator.num_pages)

        except EmptyPage as error:
            topic.comments = paginator.page(paginator.num_pages)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/topic/index.html", {
            "page": channel.pk,
            "user": login,
            "form": CommentForm(),
            "topic": topic,
            "channel": channel
        })


    @login_required()
    def page(request, channel_id, topic_id):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : int
            Channel primary key identifier
        topic_id : int
            Topic primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        channel = get_object_or_404(Channel, pk=channel_id)

        topic = get_object_or_404(Topic, pk=topic_id)

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(reverse(
            "topic_page", args=( channel.identifier, topic.identifier, page )))


    @login_required()
    def add(request, channel_id):
        """ Add a new topic to an existing channel

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : int
            Channel primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.add_topic"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        channel = get_object_or_404(Channel, pk=channel_id)

        if request.method == "POST":
            form = TopicForm(request.POST)

            if form.is_valid():

                try:
                    pk = str(Topic.objects.latest("pk").pk + 1)

                except ObjectDoesNotExist as error:
                    pk = '1'

                # Generate topic identifier
                form.cleaned_data["identifier"] = generate_identifier(
                    form.cleaned_data["title"])

                form.cleaned_data["user"] = login
                form.cleaned_data["updater"] = login
                form.cleaned_data["channel"] = channel

                topic = Topic(**form.cleaned_data)
                topic.save()

                return HttpResponseRedirect(reverse("topic_look",
                    args=( topic.identifier, topic.pk )))

        else:
            form = TopicForm()

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/topic/edit.html", {
            "page": channel.pk,
            "user": login,
            "form": form,
            "channel": channel
        })


    @login_required()
    def edit(request, topic_id):
        """ Edit an existing topic

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        topic_id : int
            Topic primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.change_topic"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        topic = get_object_or_404(Topic, pk=topic_id)

        if not login.pk == topic.user.pk:
            if not login.is_staff:
                return HttpResponseRedirect(reverse("error"))

        if request.method == "POST":
            form = TopicForm(request.POST)

            if form.is_valid():
                pk = str(topic.pk)

                form.cleaned_data["updater"] = login

                for key, value in form.cleaned_data.items():
                    setattr(topic, key, value)

                # Generate topic identifier
                topic.identifier = generate_identifier(topic.title)

                topic.save()

                return HttpResponseRedirect(reverse("topic_look",
                    args=( topic.identifier, topic.pk )))

        else:
            form = TopicForm(instance=topic)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/topic/edit.html", {
            "page": topic.channel.pk,
            "user": login,
            "form": form,
            "topic": topic
        })


    @login_required()
    def delete(request, topic_id):
        """ Delete an existing topic

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        topic_id : int
            Topic primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.delete_topic"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        topic = get_object_or_404(Topic, pk=topic_id)

        if request.method == "POST":
            channel_id = topic.channel.identifier

            # Remove channel and all his data
            if request.POST["choice"] == "yes":
                topic.delete()

                return HttpResponseRedirect(reverse("channel_look",
                    args=( topic.channel.identifier, topic.channel.pk )))

            return HttpResponseRedirect(
                reverse("topic_look", args=( topic.identifier, topic.pk )))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/topic/delete.html", {
            "page": topic.channel.pk,
            "user": login,
            "topic": topic
        })


class CommentView(object):

    @login_required()
    def add(request, topic_id):
        """ Add a new comment to an existing topic

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        topic_id : int
            Topic primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.add_comment"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        topic = get_object_or_404(Topic, pk=topic_id)

        if request.method == "POST":
            form = CommentForm(request.POST)

            if form.is_valid():
                form.cleaned_data["user"] = login
                form.cleaned_data["updater"] = login

                form.cleaned_data["topic"] = topic
                form.cleaned_data["index"] = len(
                    Comment.objects.filter(topic=topic)) + 1

                comment = Comment(**form.cleaned_data)
                comment.save()

                return HttpResponseRedirect(reverse("topic_look",
                    args=( topic.identifier, topic.pk )))

        else:
            form = CommentForm()

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/comment/edit.html", {
            "page": topic.channel.pk,
            "user": login,
            "form": form
        })


    @login_required()
    def edit(request, comment_id):
        """ Edit an existing comment

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        comment_id : int
            Comment primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.change_comment"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        comment = get_object_or_404(Comment, pk=comment_id)

        if not login.pk == comment.user.pk:
            if not login.is_staff:
                return HttpResponseRedirect(reverse("error"))

        if request.method == "POST":
            form = CommentForm(request.POST)

            if form.is_valid():
                form.cleaned_data["updater"] = login

                for key, value in form.cleaned_data.items():
                    setattr(comment, key, value)

                comment.save()

                return HttpResponseRedirect(reverse("topic_look", args=(
                    comment.topic.identifier, comment.topic.pk )))

        else:
            form = CommentForm(instance=comment)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/comment/edit.html", {
            "page": comment.topic.channel.pk,
            "user": login,
            "form": form,
            "comment": comment
        })


    @login_required()
    def delete(request, comment_id):
        """ Delete an existing comment

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        comment_id : int
            Comment primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.delete_comment") or not login.is_staff:
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        comment = get_object_or_404(Comment, pk=comment_id)

        if request.method == "POST":
            topic = comment.topic

            if request.POST["choice"] == "yes":
                # Remove channel and all his data
                comment.delete()

            return HttpResponseRedirect(reverse("topic_look",
                args=( topic.identifier, topic.pk )))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "channel/comment/delete.html", {
            "page": comment.topic.channel.pk,
            "user": login,
            "comment": comment
        })


    @login_required()
    def switch(request, comment_id):
        """ Switch comment visible status

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        comment_id : int
            Comment primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("channel.change_comment"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        comment = get_object_or_404(Comment, pk=comment_id)

        comment.updater = login

        comment.hide = not comment.hide
        comment.save()

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(reverse("topic_look", args=(
            comment.topic.identifier, comment.topic.pk )))
