# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django import template
from django.urls import reverse
from django.conf import settings
from django.utils.timezone import localtime
from django.utils.translation import ugettext as _
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.template.defaultfilters import stringfilter

# Pebble
from engine.pages.channel.models import Channel

# ------------------------------------------------------------------------------
#   Filter
# ------------------------------------------------------------------------------

register = template.Library()

@register.simple_tag
def get_channels():
    """ Retrieve available channels list

    Returns
    -------
    dict
        Channels list ordered by categories
    """

    channels = dict()

    for channel in Channel.objects.all().order_by("category__title", "title"):

        key = _("Main")
        if channel.category is not None:
            key = channel.category.title

        if not key in channels.keys():
            channels[key] = list()

        channels[key].append(channel)

    return channels
