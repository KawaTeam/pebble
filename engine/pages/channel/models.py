# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models

from django.contrib.auth.models import User
from django.contrib.auth.models import Group

# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Category(models.Model):

    title = models.CharField(max_length=32)

    identifier = models.CharField(max_length=512)


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return self.title


class Channel(models.Model):

    type = "channel"

    title = models.CharField(max_length=32)

    identifier = models.CharField(max_length=512)

    description = models.CharField(max_length=500, blank=True)

    group = models.ForeignKey(Group, null=True, blank=True)

    category = models.ForeignKey(
        Category, blank=True, null=True, on_delete=models.CASCADE)


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return "%s - %s" % (self.identifier, self.title)


class Topic(models.Model):

    type = "topic"

    title = models.CharField(max_length=256)

    identifier = models.CharField(max_length=512)

    content = models.TextField()

    lock = models.BooleanField(default=False)
    stick = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="topic_user")
    updater = models.ForeignKey(
        User, blank=True, null=True, related_name="topic_updater")


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return "%s from %s" % (self.title, self.user.username)


class Comment(models.Model):

    type = "comment"

    content = models.TextField()

    hide = models.BooleanField(default=False)

    index = models.PositiveIntegerField(default=1)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="comment_user")
    updater = models.ForeignKey(
        User, blank=True, null=True, related_name="comment_updater")


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return "%s's comment on %s's topic" % (
            self.user.username, self.topic.title)
