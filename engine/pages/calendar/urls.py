# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf.urls import url
from django.conf.urls import include

# Pebble
from engine.pages.calendar.views import CalendarView
from engine.pages.calendar.views import CalendarDateView

# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [

    # ----------------------------------------
    #   Actions
    # ----------------------------------------

    url(r'^action/event/', include([
        url(r'^add/$',
            CalendarView.add, name="action_event_add"),
        url(r'^edit/(?P<event_id>[\d]+)/$',
            CalendarView.edit, name="action_event_edit"),
        url(r'^delete/(?P<event_id>[\d]+)/$',
            CalendarView.delete, name="action_event_delete"),
        url(r'^page/$',
            CalendarView.page, name="action_event_page"),
        url(r'^register/(?P<date_id>[\d]+)/$',
            CalendarView.register, name="action_event_register")
        ])
    ),

    url(r'^action/date/', include([
        url(r'^add/(?P<event_id>[\d]+)/$',
            CalendarDateView.add, name="action_date_add"),
        url(r'^delete/(?P<date_id>[\d]+)/$',
            CalendarDateView.delete, name="action_date_delete")
        ])
    ),

    # ----------------------------------------
    #   Views
    # ----------------------------------------

    url(r'^$',
        CalendarView.index, name="calendar_home"),

    url(r'^(?P<page>\d+)/$',
        CalendarView.index, name="calendar_page"),

    url(r'^(?P<identifier>[^\s\/]+)-(?P<index>[\d]+)/$',
        CalendarView.view, name="calendar_look")
]
