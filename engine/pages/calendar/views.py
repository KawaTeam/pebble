# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django import forms

from django.conf import settings

from django.urls import reverse

from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.shortcuts import get_object_or_404

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from django.core.exceptions import ObjectDoesNotExist

from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required

# Pebble
from engine.common import generate_identifier

from engine.pages.calendar.models import Event
from engine.pages.calendar.models import EventDate

# Regex
from re import sub as re_sub

# ------------------------------------------------------------------------------
#   Forms
# ------------------------------------------------------------------------------

class EventForm(forms.ModelForm):

    class Meta:

        model = Event

        fields = [
            "closed",
            "title",
            "description"
        ]


class EventDateForm(forms.ModelForm):

    class Meta:

        model = EventDate

        fields = [
            "date"
        ]

        widgets = {
            "date": forms.DateInput(attrs={
                "placeholder": "YYYY-MM-DD" }),
        }

# ------------------------------------------------------------------------------
#   Views
# ------------------------------------------------------------------------------

class CalendarView(object):

    @login_required()
    def index(request, page=None):
        """ Show channel view

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        identifier : str
            Channel identifier
        index : int
            Channel index
        page : int, optional
            Paginator current page
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        paginator = Paginator(Event.objects.order_by("closed", "-updated"),
            settings.PAGES["calendar"])

        # Load topics for specified page
        try:
            events = paginator.page(page)

        except PageNotAnInteger as error:
            events = paginator.page(1)

        except EmptyPage as error:
            events = paginator.page(paginator.num_pages)

        # Retrieve dates number for every accessible events
        for event in events:
            event.dates = EventDate.objects.filter(event=event.pk)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "calendar/index.html", {
            "page": "calendar",
            "events": events,
            "user": login
        })


    @login_required()
    def page(request):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(
            reverse("calendar_page", args=( page, )))


    @login_required()
    def register(request, date_id):
        """ Register to a specific date

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        date_id : int
            EventDate primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        event_date = get_object_or_404(EventDate, pk=date_id)

        if not login in event_date.users.all():
            event_date.users.add(login)

        else:
            event_date.users.remove(login)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(reverse("calendar_look",
            args=( event_date.event.identifier, event_date.event.pk )))


    @login_required()
    def view(request, identifier, index):
        """ View a specific calendar

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        identifier : str
            Game identifier
        index : int
            Game index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        event = get_object_or_404(Event, pk=index)

        # Avoid to list unactive users
        event.dates = EventDate.objects.filter(event=event.pk).order_by("date")

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "calendar/view.html", {
            "page": "calendar",
            "user": login,
            "event": event
        })


    @login_required()
    def add(request):
        """ Add a new event

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("calendar.add_event"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        if request.method == "POST":
            form = EventForm(request.POST)

            if form.is_valid():
                event = Event()

                event.title = form.cleaned_data["title"]
                event.closed = form.cleaned_data["closed"]
                event.description = form.cleaned_data["description"]
                event.identifier = generate_identifier(event.title)
                event.updater = login
                event.user = login

                event.save()

                return HttpResponseRedirect(reverse("calendar_look",
                    args=( event.identifier, event.pk )))

        else:
            form = EventForm()

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "calendar/event/edit.html", {
            "page": "calendar",
            "user": login,
            "form": form
        })


    @login_required()
    def edit(request, event_id):
        """ Edit an existing event

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        event_id : int
            Event primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("calendar.change_event"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        event = get_object_or_404(Event, pk=event_id)

        if request.method == "POST":
            form = EventForm(request.POST)

            if form.is_valid():
                form.cleaned_data["updater"] = login

                for key, value in form.cleaned_data.items():
                    setattr(event, key, value)

                # Generate event identifier
                event.identifier = generate_identifier(event.title)

                event.save()

                return HttpResponseRedirect(reverse("calendar_look",
                    args=( event.identifier, event.pk )))

        else:
            form = EventForm(instance=event)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "calendar/event/edit.html", {
            "page": "calendar",
            "user": login,
            "form": form,
            "event": event
        })


    @login_required()
    def delete(request, event_id):
        """ Delete an existing comment

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        event_id : int
            Event primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("calendar.delete_event") or not login.is_staff:
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        event = get_object_or_404(Event, pk=event_id)

        if request.method == "POST":

            if request.POST["choice"] == "yes":
                # Remove event
                event.delete()

            return HttpResponseRedirect(reverse("calendar_home"))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "calendar/event/delete.html", {
            "page": "calendar",
            "user": login,
            "event": event
        })


class CalendarDateView(object):

    @login_required()
    def add(request, event_id):
        """ Add a new date to a specific event

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("calendar.add_eventdate"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        event = get_object_or_404(Event, pk=event_id)

        if request.method == "POST":
            form = EventDateForm(request.POST)

            if form.is_valid():
                event_date = EventDate()

                event_date.date = form.cleaned_data["date"]
                event_date.event = event
                event_date.updater = login
                event_date.user = login

                event_date.save()

                event_date.users.add(login)

                event_date.save()


                return HttpResponseRedirect(reverse("calendar_look",
                    args=( event_date.event.identifier, event_date.event.pk )))

        else:
            form = EventDateForm()

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "calendar/date/edit.html", {
            "page": "calendar",
            "user": login,
            "form": form,
            "event": event
        })


    @login_required()
    def delete(request, date_id):
        """ Delete an existing comment

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        date_id : int
            EventDate primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("calendar.delete_event") or not login.is_staff:
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        event_date = get_object_or_404(EventDate, pk=date_id)

        if request.method == "POST":

            if request.POST["choice"] == "yes":
                # Remove event
                event_date.delete()

            return HttpResponseRedirect(reverse("calendar_look",
                args=( event_date.event.identifier, event_date.event.pk )))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "calendar/date/delete.html", {
            "page": "calendar",
            "user": login,
            "date": event_date
        })
