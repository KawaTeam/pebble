# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models

from django.contrib.auth.models import User

# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Event(models.Model):

    type = "calendar"

    title = models.CharField(max_length=256)

    identifier = models.CharField(max_length=512)

    description = models.TextField(blank=True)

    closed = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="event_user")
    updater = models.ForeignKey(
        User, blank=True, null=True, related_name="event_updater")


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return self.title


class EventDate(models.Model):

    date = models.DateField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    users = models.ManyToManyField(
        User, related_name="date_users", blank=True)

    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="date_user")
    updater = models.ForeignKey(
        User, blank=True, null=True, related_name="date_updater")


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return self.user.username
