# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf.urls import url
from django.conf.urls import include

from django.contrib import admin

# Pebble
from engine.pages.common.views import AuthView
from engine.pages.common.views import CommonView

# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [

    # ----------------------------------------
    #   Actions
    # ----------------------------------------

    url(r'^home/', include([
        url(r'^action/page/$',
            CommonView.page, name="action_home_page")
    ])),

    # ----------------------------------------
    #   Views
    # ----------------------------------------

    url(r'^$',
        CommonView.index, name="root"),

    url(r'^team/$',
        CommonView.team, name="team"),

    url(r'^profile/(?P<username>[^\s\/]+)/$',
        CommonView.profile, name="profile"),

    url(r'^uploads/avatar/(?P<uuid>[\w\d]+)/?$',
        CommonView.avatar, name="avatar"),

    url(r'^error/$',
        CommonView.index, name="error"),

    url(r'^home/(?P<page>[\d]+)/$',
        CommonView.index, name="home_page"),

    url(r'^settings/', include([

        # ----------------------------------------
        #   User preferences
        # ----------------------------------------

        url(r'^preferences/', include([
            url(r'^$',
                CommonView.preferences, name="preferences"),
            url(r'^(?P<option>[\w]+)/$',
                CommonView.preferences, name="option")
        ])),

        # ----------------------------------------
        #   Administration
        # ----------------------------------------

        url(r'^admin/',
            admin.site.urls)
    ])),

    url(r'^auth/', include([
        url(r'^register/$',
            AuthView.register, name="register"),
        url(r'^sign_in/$',
            AuthView.sign_in, name="sign_in"),
        url(r'^sign_out/$',
            AuthView.sign_out, name="sign_out"),
    ])),
]
