# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf import settings

from django.urls import reverse

from django.http import FileResponse
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.shortcuts import get_object_or_404

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from django.core.exceptions import ObjectDoesNotExist

from django.utils.translation import ugettext as _

from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import login as dlogin
from django.contrib.auth import logout as dlogout
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

# Filesystem
import magic

from pathlib import Path

from os.path import join as path_join

# Pebble
from engine.common import is_allowed

from engine.pages.game.models import Game
from engine.pages.upload.models import File
from engine.pages.server.models import Server
from engine.pages.static.models import StaticPage
from engine.pages.channel.models import Topic
from engine.pages.channel.models import Channel
from engine.pages.channel.models import Comment
from engine.pages.common.models import get_themes
from engine.pages.common.models import Informations
from engine.pages.calendar.models import Event

# ------------------------------------------------------------------------------
#   Views
# ------------------------------------------------------------------------------

class CommonView(object):

    @login_required()
    def index(request, page=None):
        """

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Latest data
        # ----------------------------------------

        notifications = list()

        notifications.extend(
            Topic.objects.order_by("-updated"))
        notifications.extend(
            Comment.objects.exclude(hide=True).order_by("-updated"))

        if "server" in settings.AVAILABLE_MODULES:
            notifications.extend(
                Server.objects.order_by("-updated"))

        if "game" in settings.AVAILABLE_MODULES:
            notifications.extend(
                Game.objects.order_by("-updated", "title"))

        if "static" in settings.AVAILABLE_MODULES:
            notifications.extend(
                StaticPage.objects.exclude(draft=True).order_by("-updated"))

        if "upload" in settings.AVAILABLE_MODULES:
            notifications.extend(
                File.objects.exclude(private=True).order_by("-updated"))

        if "calendar" in settings.AVAILABLE_MODULES:
            notifications.extend(
                Event.objects.exclude(closed=True).order_by("-updated"))

        # ----------------------------------------
        #   Latest post
        # ----------------------------------------

        paginator = Paginator(
            sorted(notifications, key=lambda k: k.updated, reverse=True),
            settings.PAGES["notification"])

        # Load topics for specified page
        try:
            notifications = paginator.page(page)

        except PageNotAnInteger as error:
            notifications = paginator.page(1)

        except EmptyPage as error:
            notifications = paginator.page(paginator.num_pages)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "common/index.html", {
            "page": "home",
            "user": login,
            "notifications": notifications
        })


    @login_required()
    def page(request):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : int
            Channel primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(
            reverse("home_page", args=( page, )))


    @login_required()
    def team(request, username=None):
        """

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "common/team.html", {
            "page": "team",
            "user": login,
            "team": User.objects.all().order_by("username")
        })


    @login_required()
    def profile(request, username):
        """

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        user = get_object_or_404(User, username=username)

        topics = len(Topic.objects.filter(user=user))
        comments = len(Comment.objects.filter(user=user))

        servers = int()
        if "server" in settings.AVAILABLE_MODULES:
            servers = len(Server.objects.filter(user=user))

        games = int()
        if "game" in settings.AVAILABLE_MODULES:
            games = len(Game.objects.filter(users__pk=user.pk))

        pages = int()
        if "static" in settings.AVAILABLE_MODULES:
            pages = len(
                StaticPage.objects.exclude(draft=True).filter(user=user))

        files = int()
        if "upload" in settings.AVAILABLE_MODULES:
            files = len(File.objects.exclude(private=True).filter(user=user))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "common/profile.html", {
            "profil": user,
            "user": login,
            "topics": topics,
            "comments": comments,
            "servers": servers,
            "games": games,
            "files": files,
            "pages": pages
        })


    @login_required()
    def preferences(request, option="parameters"):
        """

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        # Check user status
        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        if request.method == "POST":

            # ----------------------------------------
            #   Avatar
            # ----------------------------------------

            if option == "avatar":
                error = False

                keys = ["new"]

                for key in keys:
                    if not '_'.join((key, option)) in request.FILES:
                        error = True

                # Avoid to continue if a key is missing
                if error:
                    messages.add_message(request, messages.ERROR,
                        _("Form has been altered. Abort."))

                else:
                    uploaded_file = request.FILES["new_avatar"]

                    # Convert file size from bytes to mebibytes
                    uploaded_file_size = uploaded_file.size / pow(1024, 2)

                    # Check if uploaded file size is under maximum allowed size
                    if uploaded_file_size <= settings.MAXIMUM_AVATAR_SIZE:

                        # Check if uploaded file can be uploaded on server
                        if is_allowed(uploaded_file.content_type, True):

                            # Try to add the new item to database
                            try:
                                login.informations.avatar = uploaded_file
                                login.informations.save(
                                    update_fields=["avatar"])

                                messages.add_message(request, messages.INFO,
                                    _("Your avatar has been updated with "
                                    "success."))

                            # Uploader try to send a file with another mimetype
                            except TypeError as error:
                                messages.add_message(request, messages.ERROR,
                                    _("You cannot fool the server !"))

                        # Unsupported extension
                        else:
                            messages.add_message(request, messages.ERROR,
                                _("File extension is not supported."))

                            messages.add_message(request, messages.WARNING,
                                _("Supported extensions: %s" % ", ".join(sorted(
                                set(settings.ALLOWED_AVATAR_EXTENSIONS.values())
                                ))))

                    # Oversized file
                    else:
                        messages.add_message(request, messages.WARNING,
                            _("File size cannot exceded %.2fMiB") % \
                            settings.MAXIMUM_AVATAR_SIZE)

            # ----------------------------------------
            #   Profile informations
            # ----------------------------------------

            elif option == "informations":
                error = False

                keys = ["jabber", "email", "website",
                    "diaspora", "mastodon", "description"]

                for key in keys:
                    if not '_'.join((key, option)) in request.POST:
                        error = True

                # Avoid to continue if a key is missing
                if error:
                    messages.add_message(request, messages.ERROR,
                        _("Form has been altered. Abort."))

                # Update profile
                else:
                    for key in keys:
                        setattr(login.informations,
                            key, request.POST['_'.join((key, option))])

                    login.informations.save(update_fields=keys)

                    messages.add_message(request, messages.INFO,
                        _("Your informations has been updated with success."))

            # ----------------------------------------
            #   Website parameters
            # ----------------------------------------

            elif option == "parameters":
                error = False

                keys = ["theme"]

                for key in keys:
                    if not '_'.join((key, option)) in request.POST:
                        error = True

                # Avoid to continue if a key is missing
                if error:
                    messages.add_message(request, messages.ERROR,
                        _("Form has been altered. Abort."))

                # Update profile
                else:
                    for key in keys:
                        setattr(login.informations,
                            key, request.POST['_'.join((key, option))])

                    login.informations.save(update_fields=keys)

                    messages.add_message(request, messages.INFO,
                        _("Your settings has been updated with success."))

            # ----------------------------------------
            #   Password
            # ----------------------------------------

            elif option == "password":
                error = False

                keys = ["current", "new", "check"]

                for key in keys:
                    if not '_'.join((key, option)) in request.POST:
                        error = True

                # Avoid to continue if a key is missing
                if error:
                    messages.add_message(request, messages.ERROR,
                        _("Form has been altered. Abort."))

                # Check if specified password can be used
                elif len(request.POST["current_password"]) > 0:

                    if login.check_password(request.POST["current_password"]):
                        new_password = request.POST["new_password"]
                        check_password = request.POST["check_password"]

                        if len(new_password) > 0 and len(check_password) > 0:

                            if new_password == check_password:
                                login.set_password(new_password)
                                login.save(update_fields=["password"])

                                # Avoid to logout user when password was changed
                                update_session_auth_hash(request, login)

                                messages.add_message(request, messages.INFO,
                                    _("Your password has been updated with "
                                    "success."))

                            else:
                                messages.add_message(request, messages.ERROR,
                                    _("Password verification has failed."))

                        else:
                            messages.add_message(request, messages.ERROR,
                                _("You cannot use an empty string as new "
                                "password."))

                    else:
                        messages.add_message(request, messages.ERROR,
                            _("This is not your current password."))

                else:
                    messages.add_message(request, messages.ERROR,
                        _("You need to specify your current password if you "
                        "want to update it."))

        # ----------------------------------------
        #   Additional data
        # ----------------------------------------

        data = None

        if option == "parameters":
            data = get_themes()

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "common/preferences.html", {
            "page": "preferences",
            "option": option,
            "user": login,
            "data": data
        })


    @login_required()
    def avatar(request, uuid):
        """ Retrieve avatar file from a specific uuid

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        uuid : str
            Avatar UUID file name
        """

        login = request.user

        # Check user status
        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        path = Path(settings.MEDIA_ROOT, "avatar", uuid)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        if path.exists():

            return FileResponse(path.open(mode='rb'),
                content_type=magic.from_file(str(path), mime=True))

        return HttpResponse(status=404)


class AuthView(object):

    def register(request):
        """

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "auth/register.html")


    def sign_in(request):
        """

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        if request.method == "POST":

            user = authenticate(
                username=request.POST["username"],
                password=request.POST["password"])

            # Check if current user exists and can login
            if user is not None and user.is_active:
                dlogin(request, user)

                return HttpResponseRedirect("/")

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "auth/sign_in.html", {
            "form": AuthenticationForm(request) })


    @login_required()
    def sign_out(request):
        """

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        dlogout(request)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect("/")
