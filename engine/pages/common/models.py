# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models
from django.db.models.signals import post_save

from django.conf import settings

from django.dispatch import receiver

from django.contrib.auth.models import User

from django.core.files.storage import FileSystemStorage

# Filesystem
from os import remove

from pathlib import Path

# Pebble
from engine.common import is_allowed
from engine.common import get_mimetype

# ------------------------------------------------------------------------------
#   Functions
# ------------------------------------------------------------------------------

def upload_data(profile, filename):
    """ Upload an avatar

    This function define the user avatar file and replace the existing one

    Parameters
    ----------
    profile : user.models.Informations
        The user profile
    filename : str
        New avatar filename

    Returns
    -------
    str
        New avatar path in upload directory
    """

    mime = get_mimetype(profile.avatar.file)

    if is_allowed(mime):
        extension = settings.ALLOWED_AVATAR_EXTENSIONS[mime]

        try:
            from hashlib import blake2b

            name = blake2b(digest_size=32)

        except ImportError:
            from hashlib import sha256

            name = sha256()

        name.update(bytes(profile.user.username, "utf-8"))

        path = Path("avatar", "%s.%s" % (name.hexdigest(), extension))

        media_path = Path(settings.MEDIA_ROOT)

        if media_path.joinpath(path).exists():
            media_path.joinpath(path).unlink()

        return path

    return None


def get_themes():
    """ Retrieve available theme
    """

    theme_path = Path(settings.STATIC_DIR).joinpath("css", "themes")

    data = list()

    for filename in theme_path.glob("*.css"):
        data.append((filename.stem, filename.stem))

    return tuple(data)

# ------------------------------------------------------------------------------
#   Signals
# ------------------------------------------------------------------------------

@receiver(post_save, sender=User)
def on_post_save(sender, **kwargs):
    """ Generate informations model for new users

    This function generate the correct Informations model which is stack inside
    the default User model
    """

    if kwargs["created"]:
        kwargs["instance"].informations = Informations(pk=kwargs["instance"].pk)
        kwargs["instance"].informations.save()

# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Informations(models.Model):
    """ Represent user's informations
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    description = models.CharField(max_length=500, blank=True)

    jabber = models.EmailField(blank=True)
    email = models.EmailField(blank=True)

    diaspora = models.URLField(blank=True)
    mastodon = models.URLField(blank=True)
    website = models.URLField(blank=True)

    theme = models.CharField(
        max_length=64, default="default", choices=get_themes())

    avatar = models.ImageField(
        upload_to=upload_data, default="images/default.png")


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return "%s's informations" % self.user.username


    @property
    def type(self):
        """ Return model type
        """

        return "informations"
