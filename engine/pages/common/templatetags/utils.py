# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django import template
from django.urls import reverse
from django.conf import settings
from django.utils.timezone import localtime
from django.utils.translation import ugettext as _
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.template.defaultfilters import stringfilter

# Datetime
from datetime import date
from datetime import datetime
from datetime import timezone
from datetime import timedelta

# Filesystem
from os.path import exists
from os.path import basename
from os.path import expanduser
from os.path import join as path_join

# Markdown
from markdown import markdown

# Regex
from re import compile as re_compile

# System
from os import environ
from sys import exit as sys_exit

# ------------------------------------------------------------------------------
#   Filter
# ------------------------------------------------------------------------------

register = template.Library()

@register.filter
@stringfilter
def avatar(image):
    """ Return default avatar if no avatar was registered
    """

    if len(image) == 0 or image.endswith("default.png"):
        image = "/static/images/default.png"

    return image


@register.filter
@stringfilter
def design(theme):
    """ Return user theme or default one
    """

    if len(theme) == 0:
        theme = "default"

    return theme


@register.filter
def index(data, element):
    """ Return element index in data structure
    """

    if element in data:
        return list(data).index(element)

    return None


@register.filter
@register.simple_tag
def setting(key, default=None):
    """ Retrieve a specific option from instance settings

    Parameters
    ----------
    key : str
        Option key name
    default : str
        Default value to use if the specified key not exist (Default: None)

    Returns
    -------
    str or None
        Setting value
    """

    if settings.CONFIGURATION.has_option("metadata", key):
        return settings.CONFIGURATION.get("metadata", key)

    if default is not None:
        return default

    return key


@register.filter
def is_activated(name):
    """ Check if a specific module has been activated

    Parameters
    ----------
    name : str
        Module name to check in available modules list

    Returns
    -------
    bool
        Activated module status
    """

    return name in settings.AVAILABLE_MODULES


@register.simple_tag
def get_activated_modules():
    """ Retrieve activated modules list

    Returns
    -------
    list
        Activated modules list
    """

    return list(settings.AVAILABLE_MODULES)


@register.filter
def get_module_sidebar_path(name):
    """ Retrieve sidebar template path from a specific module

    Parameters
    ----------
    name : str
        Module name
    """

    return "%s/sidebar.html" % name


@register.simple_tag
def get_comment_page(index):
    """ Retrieve comment page from his index

    Parameters
    ----------
    index : int
        Comment index

    Returns
    -------
    int
        Comment's page
    """

    return int(index / settings.PAGES["comment"]) + 1


@register.filter
def istoday(date_object):
    """ Check if a date if today

    Parameters
    ----------
    date_object : datetime.datetime
        Date to compare with NOW()

    Returns
    -------
    bool
        Today status
    """

    today = datetime.now(timezone.utc)

    return date_object.day == today.day and \
        date_object.month == today.month and \
        date_object.year == today.year


@register.filter
def prettydate(date_object):
    """ Convert a datetime to a pretty string

    Get a pretty string from the interval between NOW() and the wanted date

    Parameters
    ----------
    date_object : datetime.datetime
        Date to compare with NOW()

    Returns
    -------
    str or None
        Convert value
    """

    if date_object is None:
        return None

    date_now = datetime.now(timezone.utc)

    # Get days interval
    days = (date_now - date_object).days

    # Check if the specified date is today
    today = date_now.day == date_object.day

    if days == 0 and today:
        return localtime(date_object).strftime(_("Today, %H∶%M"))

    elif (days == 0 and not today) or days == 1:
        return localtime(date_object).strftime(_("Yesterday, %H∶%M"))

    return date_object


@register.filter
def markdown_to_html(text):
    """ Convert a markdown text to html output

    Parameters
    ----------
    text : str
        Text using markdown format

    Returns
    -------
    str
        Converted text
    """

    regex = re_compile(r'(@{1}[\w\d\-\_\+\@\.]+)\b')

    for element in regex.findall(text):
        username = element[1:]

        try:
            user = User.objects.get(username=username)

            if user.is_active:
                text = text.replace(element, "[%s](%s)" % (
                    element, reverse("profile", args=( user.username, ))))

        except ObjectDoesNotExist as error:
            pass

    return markdown(text, extensions=[
            "markdown.extensions.extra",
            "markdown.extensions.nl2br",
            "markdown.extensions.codehilite",
            "markdown.extensions.wikilinks",
        ])
