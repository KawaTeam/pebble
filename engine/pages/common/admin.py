# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.contrib import admin

from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

# Pebble
from engine.pages.common.models import Informations

# ------------------------------------------------------------------------------
#   Admin
# ------------------------------------------------------------------------------

class UserInformations(admin.StackedInline):
    """ User informations model
    """

    model = Informations

    can_delete = False

    verbose_name_plural = "informations"


class CustomUser(BaseUserAdmin):
    """ Alter default user model to add informations
    """

    inlines = ( UserInformations, )

# ------------------------------------------------------------------------------
#   Manager
# ------------------------------------------------------------------------------

# Remove previous User
admin.site.unregister(User)
# Store altered User with Profile
admin.site.register(User, CustomUser)
