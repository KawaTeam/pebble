# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf.urls import url
from django.conf.urls import include

# Pebble
from engine.pages.game.views import GameView

# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [

    # ----------------------------------------
    #   Actions
    # ----------------------------------------

    url(r'^action/game/', include([
        url(r'^add/$',
            GameView.add, name="action_game_add"),
        url(r'^edit/(?P<game_id>[\d]+)/$',
            GameView.edit, name="action_game_edit"),
        url(r'^delete/(?P<game_id>[\d]+)/$',
            GameView.delete, name="action_game_delete"),
        url(r'^page/$',
            GameView.page, name="action_game_page"),
        url(r'^register/(?P<game_id>[\d]+)/$',
            GameView.register, name="action_game_register")
        ])
    ),

    # ----------------------------------------
    #   Views
    # ----------------------------------------

    url(r'^$',
        GameView.index, name="game_home"),

    url(r'^random/$',
        GameView.random, name="game_random"),

    url(r'^(?P<page>\d+)/$',
        GameView.index, name="game_page"),

    url(r'^(?P<identifier>[^\s\/]+)-(?P<index>[\d]+)/$',
        GameView.view, name="game_look")
]
