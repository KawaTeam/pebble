# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django import forms

from django.conf import settings

from django.urls import reverse

from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.shortcuts import get_object_or_404

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from django.core.exceptions import ObjectDoesNotExist

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from django.utils.translation import ugettext as _

# Pebble
from engine.common import generate_identifier

from engine.pages.channel.models import Channel
from engine.pages.server.models import Server
from engine.pages.game.models import Game

# Random
from random import randint

# ------------------------------------------------------------------------------
#   Forms
# ------------------------------------------------------------------------------

class GameForm(forms.ModelForm):

    class Meta:

        model = Game

        fields = [
            "hostable",
            "title",
            "server",
            "website",
            "platforms",
            "distributors",
            "description"
        ]

# ------------------------------------------------------------------------------
#   Views
# ------------------------------------------------------------------------------

class GameView(object):

    @login_required()
    def index(request, page=None):
        """ List available games

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        page : int or None, optional
            Games list paginator index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        paginator = Paginator(Game.objects.order_by("title"),
            settings.PAGES["game"])

        # Load topics for specified page
        try:
            games = paginator.page(page)

        except PageNotAnInteger as error:
            games = paginator.page(1)

        except EmptyPage as error:
            games = paginator.page(paginator.num_pages)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "game/index.html", {
            "page": "games",
            "user": login,
            "games": games
        })


    @login_required()
    def random(request):
        """ Show a random game from database storage

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        games = Game.objects.filter(users__pk=login.pk)

        if len(games) > 0:
            game = randint(1, len(games))

            return HttpResponseRedirect(reverse("game_look",
                args=( games[game - 1].identifier, games[game - 1].pk )))

        return HttpResponseRedirect(reverse("game_home"))


    @login_required()
    def page(request):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(
            reverse("game_page", args=( page, )))


    @login_required()
    def register(request, game_id):
        """ Register to a specific game

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        game_id : int
            Page primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        game = get_object_or_404(Game, pk=game_id)

        if not login in game.users.all():
            game.users.add(login)

        else:
            game.users.remove(login)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(
            reverse("game_look", args=( game.identifier, game.pk )))


    @login_required()
    def view(request, identifier, index):
        """ View a specific game

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        identifier : str
            Game identifier
        index : int
            Game index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        game = get_object_or_404(Game, pk=index)

        # Avoid to list unactive users
        game.users = game.users.exclude(is_active=False)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "game/view.html", {
            "page": "games",
            "user": login,
            "game": game
        })


    @login_required()
    def add(request):
        """ Add a new game

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("game.add_game"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        if request.method == "POST":
            form = GameForm(request.POST)

            if form.is_valid():
                game = Game()

                game.title = form.cleaned_data["title"]
                game.server = form.cleaned_data["server"]
                game.website = form.cleaned_data["website"]
                game.hostable = form.cleaned_data["hostable"]
                game.description = form.cleaned_data["description"]
                game.identifier = generate_identifier(game.title)
                game.updater = login

                game.save()

                game.platforms = form.cleaned_data["platforms"]
                game.distributors = form.cleaned_data["distributors"]

                game.save()

                return HttpResponseRedirect(reverse("game_look",
                    args=( game.identifier, game.pk )))

        else:
            form = GameForm(initial={ "updater": login.pk })

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "game/edit.html", {
            "page": "games",
            "user": login,
            "form": form
        })


    @login_required()
    def edit(request, game_id):
        """ Edit an existing game

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        game_id : int
            Page primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("game.change_game"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        game = get_object_or_404(Game, pk=game_id)

        if request.method == "POST":
            form = GameForm(request.POST)

            if form.is_valid():
                form.cleaned_data["updater"] = login

                for key, value in form.cleaned_data.items():
                    setattr(game, key, value)

                # Generate game identifier
                game.identifier = generate_identifier(game.title)

                game.save()

                return HttpResponseRedirect(reverse("game_look",
                    args=( game.identifier, game.pk )))

        else:
            form = GameForm(instance=game)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "game/edit.html", {
            "page": "games",
            "user": login,
            "form": form,
            "game": game
        })


    @login_required()
    def delete(request, game_id):
        """ Delete an existing comment

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        game_id : int
            Page primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("game.delete_game") or not login.is_staff:
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        game = get_object_or_404(Game, pk=game_id)

        if request.method == "POST":

            if request.POST["choice"] == "yes":
                # Remove game
                game.delete()

            return HttpResponseRedirect(reverse("game_home"))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "game/delete.html", {
            "page": "games",
            "user": login,
            "game": game
        })

