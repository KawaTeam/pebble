# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models

from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator

from django.contrib.auth.models import User

# Pebble
from engine.pages.server.models import Server

# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Platform(models.Model):

    type = "platform"

    title = models.CharField(max_length=256)

    icon = models.CharField(max_length=256, blank=True)

    identifier = models.CharField(max_length=512)


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return self.title


class Distributor(models.Model):

    type = "distributor"

    title = models.CharField(max_length=256)

    identifier = models.CharField(max_length=512)

    icon = models.CharField(max_length=256, blank=True)

    website = models.URLField(blank=True)


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return self.title


class Game(models.Model):

    type = "game"

    title = models.CharField(max_length=256)

    identifier = models.CharField(max_length=512)

    description = models.TextField(blank=True)

    hostable = models.BooleanField(default=False)

    website = models.URLField(blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    platforms = models.ManyToManyField(
        Platform, related_name="game_platforms", blank=True)
    distributors = models.ManyToManyField(
        Distributor, related_name="game_distributors", blank=True)
    users = models.ManyToManyField(
        User, related_name="game_users", blank=True)

    server = models.ForeignKey(
        Server, blank=True, null=True, related_name="game_updater")
    updater = models.ForeignKey(
        User, blank=True, null=True, related_name="game_updater")


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        if self.updater is not None:
            return "%s updated by %s" % (self.title, self.updater.username)

        return self.title
