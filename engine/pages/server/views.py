# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django import forms

from django.conf import settings

from django.urls import reverse

from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.shortcuts import get_object_or_404

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from django.core.exceptions import ObjectDoesNotExist

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from django.utils.translation import ugettext as _

# Pebble
from engine.common import generate_identifier

from engine.pages.channel.models import Channel
from engine.pages.server.models import Server

# ------------------------------------------------------------------------------
#   Forms
# ------------------------------------------------------------------------------

class ServerForm(forms.ModelForm):

    class Meta:

        model = Server

        fields = [
            "active",
            "user",
            "title",
            "summary",
            "description",
            "ip_address",
            "port_address",
            "steam",
            "private",
            "password",
            "sporadic"
        ]

# ------------------------------------------------------------------------------
#   Views
# ------------------------------------------------------------------------------

class ServerView(object):

    @login_required()
    def index(request, page=None):
        """ List available servers

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        page : int or None, optional
            Servers list paginator index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        paginator = Paginator(Server.objects.order_by("-active", "title"),
            settings.PAGES["server"])

        # Load topics for specified page
        try:
            servers = paginator.page(page)

        except PageNotAnInteger as error:
            servers = paginator.page(1)

        except EmptyPage as error:
            servers = paginator.page(paginator.num_pages)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "server/index.html", {
            "page": "servers",
            "user": login,
            "servers": servers
        })


    @login_required()
    def page(request):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(
            reverse("server_page", args=( page, )))


    @login_required()
    def view(request, identifier, index):
        """ View a specific server

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        identifier : str
            Server identifier
        index : int
            Server index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        server = get_object_or_404(Server, pk=index)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "server/view.html", {
            "page": "servers",
            "user": login,
            "server": server
        })


    @login_required()
    def add(request):
        """ Add a new server

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("server.add_server"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        if request.method == "POST":
            form = ServerForm(request.POST)

            if form.is_valid():

                # Generate channel identifier
                form.cleaned_data["identifier"] = generate_identifier(
                    form.cleaned_data["title"])

                form.cleaned_data["updater"] = login

                server = Server(**form.cleaned_data)
                server.save()

                return HttpResponseRedirect(reverse("server_look",
                    args=( server.identifier, server.pk )))

        else:
            form = ServerForm(initial={ "user": login.pk })

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "server/edit.html", {
            "page": "servers",
            "user": login,
            "form": form
        })


    @login_required()
    def edit(request, server_id):
        """ Edit an existing server

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        server_id : int
            Page primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("server.change_server"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        server = get_object_or_404(Server, pk=server_id)

        if not login.pk == server.user.pk:
            if not login.is_staff:
                return HttpResponseRedirect(reverse("error"))

        if request.method == "POST":
            form = ServerForm(request.POST)

            if form.is_valid():
                form.cleaned_data["updater"] = login

                for key, value in form.cleaned_data.items():
                    setattr(server, key, value)

                # Generate server identifier
                server.identifier = generate_identifier(server.title)

                server.save()

                return HttpResponseRedirect(reverse("server_look",
                    args=( server.identifier, server.pk )))

        else:
            form = ServerForm(instance=server)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "server/edit.html", {
            "page": "servers",
            "user": login,
            "form": form,
            "server": server
        })


    @login_required()
    def delete(request, server_id):
        """ Delete an existing comment

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        server_id : int
            Page primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("server.delete_server") or not login.is_staff:
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        server = get_object_or_404(Server, pk=server_id)

        if request.method == "POST":

            if request.POST["choice"] == "yes":
                # Remove server
                server.delete()

            return HttpResponseRedirect(reverse("server_home"))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "server/delete.html", {
            "page": "servers",
            "user": login,
            "server": server
        })

