# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf.urls import url
from django.conf.urls import include

# Pebble
from engine.pages.server.views import ServerView

# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [

    # ----------------------------------------
    #   Actions
    # ----------------------------------------

    url(r'^action/server/', include([
        url(r'^add/$',
            ServerView.add, name="action_server_add"),
        url(r'^edit/(?P<server_id>[\d]+)/$',
            ServerView.edit, name="action_server_edit"),
        url(r'^delete/(?P<server_id>[\d]+)/$',
            ServerView.delete, name="action_server_delete"),
        url(r'^page/$',
            ServerView.page, name="action_server_page")
        ])
    ),

    # ----------------------------------------
    #   Views
    # ----------------------------------------

    url(r'^$',
        ServerView.index, name="server_home"),

    url(r'^(?P<page>\d+)/$',
        ServerView.index, name="server_page"),

    url(r'^(?P<identifier>[^\s\/]+)-(?P<index>[\d]+)/$',
        ServerView.view, name="server_look")
]
