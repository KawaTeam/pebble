# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django import forms

from django.conf import settings

from django.urls import reverse

from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.shortcuts import get_object_or_404

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from django.core.exceptions import ObjectDoesNotExist

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from django.utils.translation import ugettext as _

# Pebble
from engine.common import generate_identifier

from engine.pages.static.models import StaticPage
from engine.pages.channel.models import Channel

# Random
from random import randint

# Regex
from re import sub as re_sub

# ------------------------------------------------------------------------------
#   Forms
# ------------------------------------------------------------------------------

class StaticForm(forms.ModelForm):

    class Meta:

        model = StaticPage

        fields = [
            "title",
            "category",
            "content",
            "draft"
        ]

        widgets = {
            "category": forms.widgets.TextInput(attrs={"list": "categories"})
        }

# ------------------------------------------------------------------------------
#   Views
# ------------------------------------------------------------------------------

class StaticView(object):

    @login_required()
    def index(request, page=None):
        """ List available static pages

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        page : int or None, optional
            Static pages list paginator index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        paginator = Paginator(
            StaticPage.objects.exclude(draft=True).order_by("-updated"),
            settings.PAGES["static"])

        # Load topics for specified page
        try:
            pages = paginator.page(page)

        except PageNotAnInteger as error:
            pages = paginator.page(1)

        except EmptyPage as error:
            pages = paginator.page(paginator.num_pages)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "static/index.html", {
            "page": "static",
            "user": login,
            "pages": pages
        })


    @login_required()
    def random(request):
        """ Show a random static page from database storage

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        pages = StaticPage.objects.all().exclude(draft=True)

        if len(pages) > 0:
            page = randint(1, len(pages))

            return HttpResponseRedirect(reverse("static_look",
                args=( pages[page - 1].identifier, pages[page - 1].pk )))

        return HttpResponseRedirect(reverse("static_home"))


    @login_required()
    def page(request):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : int
            Channel primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(
            reverse("static_page", args=( page, )))


    @login_required()
    def view(request, identifier, index):
        """ View a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        identifier : str
            Page identifier
        index : int
            Page index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = get_object_or_404(StaticPage, pk=index)

        if page.draft and not login.pk == page.user.pk:
            return HttpResponseRedirect(reverse("static_home"))

        page_type = "static"

        if page.draft:
            page_type = "drafts"

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "static/view.html", {
            "page": "static",
            "user": login,
            "static": page
        })


    @login_required()
    def add(request):
        """ Add a new page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("static.add_staticpage"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        if request.method == "POST":
            form = StaticForm(request.POST)

            if form.is_valid():

                try:
                    pk = str(StaticPage.objects.latest("pk").pk + 1)

                except ObjectDoesNotExist as error:
                    pk = '1'

                # Generate channel identifier
                form.cleaned_data["identifier"] = generate_identifier(
                    form.cleaned_data["title"])

                # Clean category
                form.category = form.cleaned_data["category"].strip()
                if len(form.category) > 0:
                    form.category = form.category.capitalize()

                form.cleaned_data["user"] = login
                form.cleaned_data["updater"] = login

                page = StaticPage(**form.cleaned_data)
                page.save()

                return HttpResponseRedirect(reverse("static_look",
                    args=( page.identifier, page.pk )))

        else:
            form = StaticForm()

        # ----------------------------------------
        #   Retrieve categories
        # ----------------------------------------

        categories = sorted(filter(None, list(set(
            [page.category for page in StaticPage.objects.all()]))))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "static/edit.html", {
            "page": "static",
            "user": login,
            "form": form,
            "categories": categories
        })


    @login_required()
    def edit(request, page_id):
        """ Edit an existing page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        page_id : int
            Page primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("static.change_staticpage"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = get_object_or_404(StaticPage, pk=page_id)

        if not login.pk == page.user.pk:
            if not login.is_staff:
                return HttpResponseRedirect(reverse("error"))

        if request.method == "POST":
            form = StaticForm(request.POST)

            if form.is_valid():
                form.cleaned_data["updater"] = login

                for key, value in form.cleaned_data.items():
                    setattr(page, key, value)

                # Generate page identifier
                page.identifier = generate_identifier(page.title)

                # Clean category
                page.category = page.category.strip()
                if len(page.category) > 0:
                    page.category = page.category.capitalize()

                page.save()

                return HttpResponseRedirect(reverse("static_look",
                    args=( page.identifier, page.pk )))

        else:
            form = StaticForm(instance=page)

        # ----------------------------------------
        #   Retrieve categories
        # ----------------------------------------

        categories = sorted(filter(None, list(set(
            [page.category for page in StaticPage.objects.all()]))))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "static/edit.html", {
            "page": "static",
            "user": login,
            "form": form,
            "static": page,
            "categories": categories
        })


    @login_required()
    def delete(request, page_id):
        """ Delete an existing comment

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        page_id : int
            Page primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("static.delete_staticpage") or not login.is_staff:
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = get_object_or_404(StaticPage, pk=page_id)

        if request.method == "POST":

            url = "static_home"
            if page.draft:
                url = "drafts_home"

            if request.POST["choice"] == "yes":
                # Remove page
                page.delete()

            return HttpResponseRedirect(reverse(url))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "static/delete.html", {
            "page": "static",
            "user": login,
            "static": page
        })


class DraftView(object):

    @login_required()
    def index(request, page=None):
        """ List available draft pages

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        page : int or None, optional
            Draft pages list paginator index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        paginator = Paginator(StaticPage.objects.filter(
            user=login, draft=True).order_by("-updated"), 20)

        # Load topics for specified page
        try:
            pages = paginator.page(page)

        except PageNotAnInteger as error:
            pages = paginator.page(1)

        except EmptyPage as error:
            pages = paginator.page(paginator.num_pages)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "static/index.html", {
            "page": "drafts",
            "user": login,
            "pages": pages
        })


    @login_required()
    def page(request):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : int
            Channel primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(
            reverse("drafts_page", args=( page, )))
