# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf.urls import url
from django.conf.urls import include

# Pebble
from engine.pages.static.views import DraftView
from engine.pages.static.views import StaticView

# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [

    # ----------------------------------------
    #   Actions
    # ----------------------------------------

    url(r'^action/', include([
        url(r'^add/$',
            StaticView.add, name="action_static_add"),
        url(r'^edit/(?P<page_id>[\d]+)/$',
            StaticView.edit, name="action_static_edit"),
        url(r'^delete/(?P<page_id>[\d]+)/$',
            StaticView.delete, name="action_static_delete"),
        url(r'^page/', include([
            url(r'^static/$',
                StaticView.page, name="action_static_page"),
            url(r'^draft/$',
                StaticView.page, name="action_drafts_page")
            ]))
        ])
    ),

    # ----------------------------------------
    #   Views
    # ----------------------------------------

    url(r'^$',
        StaticView.index, name="static_home"),

    url(r'^random/$',
        StaticView.random, name="static_random"),

    url(r'^drafts/$',
        DraftView.index, name="drafts_home"),

    url(r'^drafts/(?P<page>\d+)/$',
        DraftView.index, name="drafts_page"),

    url(r'^(?P<page>\d+)/$',
        StaticView.index, name="static_page"),

    url(r'^(?P<identifier>[^\s\/]+)-(?P<index>[\d]+)/$',
        StaticView.view, name="static_look")
]
