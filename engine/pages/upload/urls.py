# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf.urls import url
from django.conf.urls import include

# Pebble
from engine.pages.upload.views import UploadView
from engine.pages.upload.views import PrivateView

# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [

    # ----------------------------------------
    #   Actions
    # ----------------------------------------

    url(r'^action/', include([
        url(r'^add/$',
            UploadView.add, name="action_upload_add"),
        url(r'^edit/(?P<file_id>[\d]+)/$',
            UploadView.edit, name="action_upload_edit"),
        url(r'^delete/(?P<file_id>[\d]+)/$',
            UploadView.delete, name="action_upload_delete"),
        url(r'^page/$',
            UploadView.page, name="action_upload_page")
        ])
    ),

    # ----------------------------------------
    #   Views
    # ----------------------------------------

    url(r'^$',
        UploadView.index, name="upload_home"),

    url(r'^privates/$',
        PrivateView.index, name="private_home"),

    url(r'^privates/(?P<page>\d+)/$',
        PrivateView.index, name="private_page"),

    url(r'^(?P<page>\d+)/$',
        UploadView.index, name="upload_page"),

    url(r'^(?P<identifier>[^\s\/]+)-(?P<index>[\d]+)/$',
        UploadView.view, name="upload_look")
]
