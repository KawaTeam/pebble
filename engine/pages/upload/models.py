# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models

from django.conf import settings

from django.contrib.auth.models import User

from os import remove

from pathlib import Path

# Pebble
from engine.common import is_allowed
from engine.common import get_mimetype
from engine.common import generate_identifier

# ------------------------------------------------------------------------------
#   Functions
# ------------------------------------------------------------------------------

def upload_data(instance, filename):
    """ Upload an avatar

    This function define the user avatar file and replace the existing one

    Parameters
    ----------
    instance : upload.models.File
        Database object instance
    filename : str
        New filename

    Returns
    -------
    str
        New avatar path in upload directory
    """

    extension = settings.ALLOWED_EXTENSIONS[instance.mimetype]

    # Generate a hash based on object identifier
    try:
        from hashlib import blake2b

        name = blake2b(digest_size=32)

    except ImportError:
        from hashlib import sha256

        name = sha256()

    name.update(bytes(instance.identifier, "UTF-8"))

    path = Path("file", "%s.%s" % (name.hexdigest(), extension))

    # Save file to uploads folder
    media_path = Path(settings.MEDIA_ROOT)

    # Replace an existing file
    if media_path.joinpath(path).exists():
        media_path.joinpath(path).unlink()

    return path

# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class File(models.Model):

    type = "file"

    title = models.CharField(max_length=256)

    identifier = models.CharField(max_length=512)

    description = models.CharField(max_length=500, blank=True)

    mimetype = models.CharField(max_length=512)

    private = models.BooleanField(default=False)

    path = models.FileField(upload_to=upload_data)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="file_user")


    def __str__(self):
        """ Return model representation

        Returns
        -------
        str
            Model as string
        """

        return "%s by %s" % (self.title, self.user.username)


    def __remove(self):
        """ Remove file from upload folder
        """

        path = Path(str(self.path))

        media_path = Path(settings.MEDIA_ROOT)

        if media_path.joinpath(path).exists():
            media_path.joinpath(path).unlink()


    def save(self):
        """ Save model and his data
        """

        # Generate a new identifier
        self.identifier = generate_identifier(self.title)

        # Read mimetype from uploaded file
        mime = get_mimetype(self.path.file)

        if not mime == "application/x-empty":

            # Avoid to upload unauthorized extensions
            if not is_allowed(mime):
                raise TypeError("Unauthorized mimetype")

            setattr(self, "mimetype", mime)

        super(File, self).save()


    def delete(self):
        """ Delete model and his data
        """

        self.__remove()

        super(File, self).delete()


    def replace(self, path):
        """ Replace previous file and remove it from disk

        Parameters
        ----------
        path : django.core.files.uploadedfile.InMemoryUploadedFile
            New file instance
        """

        if len(str(self.path)) > 0:
            self.__remove()

        setattr(self, "path", path)
