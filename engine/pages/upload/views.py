# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django import forms

from django.conf import settings

from django.urls import reverse

from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.shortcuts import get_object_or_404

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from django.core.exceptions import ObjectDoesNotExist

from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from django.utils.translation import ugettext as _

from django.core.files.storage import FileSystemStorage

# Pebble
from engine.common import is_allowed
from engine.common import generate_identifier

from engine.pages.channel.models import Channel
from engine.pages.upload.models import File
from engine.pages.upload.models import upload_data

# ------------------------------------------------------------------------------
#   Forms
# ------------------------------------------------------------------------------

class UploadForm(forms.ModelForm):

    class Meta:

        model = File

        fields = [
            "title",
            "description",
            "path",
            "private"
        ]

        widgets = {
            "path": forms.widgets.FileInput()
        }


class UploadEditForm(forms.ModelForm):

    class Meta:

        model = File

        fields = [
            "title",
            "description",
            "path",
            "private"
        ]

        labels = {
            "title": _("Title"),
            "description": _("Description"),
            "path": _("Replace file"),
            "private": _("Private")
        }

        widgets = {
            "path": forms.widgets.FileInput()
        }


    def __init__(self, *args, **kwargs):
        """ Constructor
        """

        super(UploadEditForm, self).__init__(*args, **kwargs)

        # It's possible to not specify a new file in edit mode
        self.fields["path"].required = False

# ------------------------------------------------------------------------------
#   Views
# ------------------------------------------------------------------------------

class UploadView(object):

    @login_required()
    def index(request, page=None):
        """ List available files

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        page : int or None, optional
            Files list paginator index
        """

        login = request.user

        # Check user status
        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        paginator = Paginator(
            File.objects.exclude(private=True).order_by("-updated"),
            settings.PAGES["upload"])

        # Load topics for specified page
        try:
            files = paginator.page(page)

        except PageNotAnInteger as error:
            files = paginator.page(1)

        except EmptyPage as error:
            files = paginator.page(paginator.num_pages)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "upload/index.html", {
            "page": "files",
            "user": login,
            "files": files
        })


    @login_required()
    def page(request):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(
            reverse("upload_page", args=( page, )))


    @login_required()
    def view(request, identifier, index):
        """ View a specific file

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        identifier : str
            File identifier
        index : int
            File index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        upload_file = get_object_or_404(File, pk=index)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "upload/view.html", {
            "page": "files",
            "user": login,
            "file": upload_file
        })


    @login_required()
    def add(request):
        """ Add a new file

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("upload.add_file"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        if request.method == "POST":
            form = UploadForm(request.POST, request.FILES)

            # Check requested form validity
            if form.is_valid():
                uploaded_file = request.FILES["path"]

                # Convert file size from bytes to mebibytes
                uploaded_file_size = uploaded_file.size / pow(1024, 2)

                # Check if uploaded file size is under maximum allowed size
                if uploaded_file_size <= settings.MAXIMUM_FILE_SIZE:

                    # Check if uploaded file can be uploaded on server
                    if is_allowed(uploaded_file.content_type):
                        form.cleaned_data["user"] = login

                        # Try to add the new item to database
                        try:
                            upload = File(**form.cleaned_data)
                            upload.save()

                            return HttpResponseRedirect(reverse("upload_look",
                                args=( upload.identifier, upload.pk )))

                        # Uploader try to send a file with another mimetype
                        except TypeError as error:
                            messages.add_message(request, messages.ERROR,
                                _("You cannot fool the server !"))

                    # Unsupported extension
                    else:
                        messages.add_message(request, messages.ERROR,
                            _("File extension is not supported."))

                        messages.add_message(request, messages.WARNING,
                            _("Supported extensions: %s" % ", ".join(
                            sorted(set(settings.ALLOWED_EXTENSIONS.values())))))

                # Oversized file
                else:
                    messages.add_message(request, messages.WARNING,
                        _("File size cannot exceded %.2fMiB") % \
                        settings.MAXIMUM_FILE_SIZE)

        else:
            form = UploadForm()

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "upload/edit.html", {
            "page": "files",
            "user": login,
            "form": form
        })


    @login_required()
    def edit(request, file_id):
        """ Edit an existing file

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        file_id : int
            Page primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("upload.change_file"):
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        error = False

        upload = get_object_or_404(File, pk=file_id)

        if not login.pk == upload.user.pk:
            if not login.is_staff:
                return HttpResponseRedirect(reverse("error"))

        if request.method == "POST":
            form = UploadEditForm(request.POST, request.FILES)

            if form.is_valid():
                form.cleaned_data["updater"] = login

                # Register new metadata
                for key, value in form.cleaned_data.items():
                    if not key == "path":
                        setattr(upload, key, value)

                # Check if a new file has been specified
                if "path" in request.FILES:
                    uploaded_file = request.FILES["path"]

                    # Convert file size from bytes to mebibytes
                    uploaded_file_size = uploaded_file.size / pow(1024, 2)

                    # Check if uploaded file size is under maximum allowed size
                    if uploaded_file_size <= settings.MAXIMUM_FILE_SIZE:

                        # Check if uploaded file can be uploaded on server
                        if is_allowed(uploaded_file.content_type):
                            upload.replace(uploaded_file)

                        # Unsupported extension
                        else:
                            messages.add_message(request, messages.ERROR,
                                _("File extension is not supported."))

                            messages.add_message(request, messages.WARNING,
                                _("Supported extensions: %s" % ", ".join(sorted(
                                set(settings.ALLOWED_EXTENSIONS.values())))))

                            error = True

                    # Oversized file
                    else:
                        messages.add_message(request, messages.WARNING,
                            _("File size cannot exceded %.2fMiB") % \
                            settings.MAXIMUM_FILE_SIZE)

                        error = True

                # Avoid to save item if an error occurs
                if not error:

                    # Try to add the new item to database
                    try:
                        upload.save()

                        return HttpResponseRedirect(reverse("upload_look",
                            args=( upload.identifier, upload.pk )))

                    # Uploader try to send a file with another mimetype
                    except TypeError as error:
                        messages.add_message(request, messages.ERROR,
                            _("You cannot fool the server !"))

        else:
            form = UploadEditForm(instance=upload)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "upload/edit.html", {
            "page": "files",
            "user": login,
            "form": form,
            "upload": upload
        })


    @login_required()
    def delete(request, file_id):
        """ Delete an existing comment

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        file_id : int
            Page primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        if not login.has_perm("upload.delete_file") or not login.is_staff:
            return HttpResponseRedirect(reverse("error"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        upload = get_object_or_404(File, pk=file_id)

        if request.method == "POST":

            url = "upload_home"
            if upload.private:
                url = "private_home"

            # Remove file
            if request.POST["choice"] == "yes":
                upload.delete()

            return HttpResponseRedirect(reverse(url))

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "upload/delete.html", {
            "page": "files",
            "user": login,
            "upload": upload
        })


class PrivateView(object):

    @login_required()
    def index(request, page=None):
        """ List available private files

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        page : int or None, optional
            Private files list paginator index
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        paginator = Paginator(File.objects.filter(
            user=login, private=True).order_by("-updated"), 20)

        # Load topics for specified page
        try:
            files = paginator.page(page)

        except PageNotAnInteger as error:
            files = paginator.page(1)

        except EmptyPage as error:
            files = paginator.page(paginator.num_pages)

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return render(request, "upload/index.html", {
            "page": "private",
            "user": login,
            "files": files
        })


    @login_required()
    def page(request):
        """ Move to a specific page

        Parameters
        ----------
        request : django.core.handlers.wsgi.WSGIRequest
            Django request
        channel_id : int
            Channel primary key identifier
        """

        login = request.user

        if not login.is_active:
            return HttpResponseRedirect(reverse("sign_out"))

        # ----------------------------------------
        #   Request
        # ----------------------------------------

        page = None

        if request.method == "POST":
            page = int(request.POST["page"])

        # ----------------------------------------
        #   Render
        # ----------------------------------------

        return HttpResponseRedirect(
            reverse("upload_page", args=( page, )))
