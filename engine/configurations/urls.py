# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf import settings

from django.conf.urls import url
from django.conf.urls import include
from django.conf.urls.static import static

# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [
    url(r'^', include("engine.pages.common.urls")),
    url(r'^channels/', include("engine.pages.channel.urls"))
]

if "static" in settings.AVAILABLE_MODULES:
    urlpatterns.append(url(r'^pages/', include("engine.pages.static.urls")))

if "upload" in settings.AVAILABLE_MODULES:
    urlpatterns.append(url(r'^files/', include("engine.pages.upload.urls")))

if "server" in settings.AVAILABLE_MODULES:
    urlpatterns.append(url(r'^servers/', include("engine.pages.server.urls")))

if "game" in settings.AVAILABLE_MODULES:
    urlpatterns.append(url(r'^games/', include("engine.pages.game.urls")))

if "calendar" in settings.AVAILABLE_MODULES:
    urlpatterns.append(url(r'^calendar/', include("engine.pages.calendar.urls")))

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
