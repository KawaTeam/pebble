# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Filesystem
from os.path import exists
from os.path import dirname
from os.path import abspath
from os.path import join as path_join

from pathlib import Path

from tempfile import gettempdir

from configparser import ConfigParser

# Secrets
try:
    from secrets import token_hex

except ImportError as error:
    from engine.common import token_hex

# System
import os

from sys import exit as sys_exit

# ------------------------------------------------------------------------------
#   Check main path
# ------------------------------------------------------------------------------

path = Path('.').resolve()

if not path.joinpath("engine", "configurations", "settings.py").exists():
    path = Path(__file__).parents[2]

# ----------------------------------------
#   Check secret key
# ----------------------------------------

secret_path = path.joinpath("data", "private")

if not secret_path.exists():
    secret_path.mkdir(0o700, parents=True)

    with secret_path.joinpath("id_key").open(mode='w') as pipe:
        pipe.write(token_hex())

# ------------------------------------------------------------------------------
#   Secret
# ------------------------------------------------------------------------------

secret_key = secret_path.joinpath("id_key")

if not secret_key.exists():
    sys_exit("Cannot found secret key file in data folder")

with secret_key.open(mode='r') as pipe:
    SECRET_KEY = ''.join(pipe.readlines())

# ------------------------------------------------------------------------------
#   Load configuration file
# ------------------------------------------------------------------------------

engine_conf = path.joinpath("data", "engine.conf")

if not engine_conf.exists():
    sys_exit("Cannot found %s file in data folder" % engine_conf.name)

CONFIGURATION = ConfigParser()
CONFIGURATION.read(str(engine_conf))

# ------------------------------------------------------------------------------
#   Settings
# ------------------------------------------------------------------------------

# Allowed hosts on website
ALLOWED_HOSTS = [ "localhost", "127.0.0.1", "[::1]" ]

for hostname in CONFIGURATION.get("engine", "hostname", fallback=str()).split():
    ALLOWED_HOSTS.append(hostname)

# Language
LANGUAGE_CODE = CONFIGURATION.get("engine", "language", fallback="en")
USE_I18N = True
USE_L10N = True

# Time zone
TIME_ZONE = CONFIGURATION.get("engine", "timezone", fallback="UTC")
USE_TZ = True

# ------------------------------------------------------------------------------
#   Debug
# ------------------------------------------------------------------------------

DEBUG = CONFIGURATION.getboolean("engine", "debug", fallback=True)

SESSION_EXPIRE_AT_BROWSER_CLOSE = False

# ------------------------------------------------------------------------------
#   Path
# ------------------------------------------------------------------------------

LOGIN_URL = "/auth/sign_in/"
MEDIA_URL = "/uploads/"
STATIC_URL = "/static/"

BASE_DIR = str(path)
LOCALE_DIR = str(path.joinpath("locale"))
STATIC_DIR = str(path.joinpath("static"))

MEDIA_ROOT = str(path.joinpath("uploads"))

LOCALE_PATHS = [ LOCALE_DIR ]
STATICFILES_DIRS = [ STATIC_DIR ]

option = CONFIGURATION.get("path", "static", fallback=None)
if option is not None and len(option) > 0:
    STATIC_ROOT = option

option = CONFIGURATION.get("path", "uploads", fallback=None)
if option is not None and len(option) > 0:
    MEDIA_ROOT = option

# ------------------------------------------------------------------------------
#   Custom options
# ------------------------------------------------------------------------------

MAXIMUM_AVATAR_SIZE = CONFIGURATION.getfloat(
    "option", "upload-avatar-size", fallback=2.0)

MAXIMUM_FILE_SIZE = CONFIGURATION.getfloat(
    "option", "upload-maximum-size", fallback=20.0)

ALLOWED_EXTENSIONS = {
    "application/pdf": "pdf",
    "audio/flac": "flac",
    "audio/midi": "midi",
    "audio/mp4": "mp4",
    "audio/mpeg": "mp3",
    "audio/ogg": "ogg",
    "audio/webm": "webm",
    "image/bmp": "bmp",
    "image/gif": "gif",
    "image/jpeg": "jpg",
    "image/png": "png",
    "text/plain": "txt",
    "video/mp4": "mp4",
    "video/mpeg": "mpeg",
    "video/ogg": "ogv",
    "video/webm": "webm"
}

ALLOWED_AVATAR_EXTENSIONS = {
    "image/bmp": "bmp",
    "image/gif": "gif",
    "image/jpeg": "jpg",
    "image/png": "png"
}

AVAILABLE_MODULES = list()

if CONFIGURATION.has_section("module"):
    for module in CONFIGURATION.options("module"):
        if CONFIGURATION.getboolean("module", module, fallback=False):
            AVAILABLE_MODULES.append(module)

PAGES = {
    "calendar": 10,
    "comment": 10,
    "notification": 15,
    "game": 20,
    "server": 20,
    "static": 20,
    "topic": 20,
    "upload": 20
}

if CONFIGURATION.has_section("page"):
    for key in CONFIGURATION.options("page"):
        PAGES[key] = CONFIGURATION.getint("page", key)

# ------------------------------------------------------------------------------
#   Applications
# ------------------------------------------------------------------------------

ROOT_URLCONF = "engine.configurations.urls"
WSGI_APPLICATION = "engine.configurations.wsgi.application"

INSTALLED_APPS = [
    # Pebble applications
    "engine.pages.calendar",
    "engine.pages.channel",
    "engine.pages.common",
    "engine.pages.game",
    "engine.pages.server",
    "engine.pages.static",
    "engine.pages.upload",
    # Django applications
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.humanize",
    "django.contrib.staticfiles"
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware"
]

# ------------------------------------------------------------------------------
#   Files
# ------------------------------------------------------------------------------

FILE_UPLOAD_HANDLERS = [
    "django.core.files.uploadhandler.MemoryFileUploadHandler"
]

FILE_UPLOAD_MAX_MEMORY_SIZE = int(MAXIMUM_FILE_SIZE * pow(1024, 2))
DATA_UPLOAD_MAX_MEMORY_SIZE = FILE_UPLOAD_MAX_MEMORY_SIZE

# ------------------------------------------------------------------------------
#   Templates
# ------------------------------------------------------------------------------

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            str(path.joinpath("templates"))
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.debug",
                "django.template.context_processors.request"
            ],
        },
    },
]

# ------------------------------------------------------------------------------
#   Database
# ------------------------------------------------------------------------------

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": str(path.joinpath("data", "pebble.sqlite3")),
    }
}

# ------------------------------------------------------------------------------
#   Password
# ------------------------------------------------------------------------------

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation." \
            "UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation." \
            "MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation." \
            "CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation." \
            "NumericPasswordValidator",
    }
]
