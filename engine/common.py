# ------------------------------------------------------------------------------
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.conf import settings

# Filesystem
import magic
import mimetypes

# Random
from random import getrandbits

# Regex
from re import sub as re_sub
from re import sub as re_sub

# Unicode
from unicodedata import normalize

# ------------------------------------------------------------------------------
#   Functions
# ------------------------------------------------------------------------------

def generate_identifier(string):
    """ Generate an identifier based on a specific string

    Parameters
    ----------
    string : str
        String to encode

    Returns
    -------
    str
        New object identifier
    """

    # Remove accent letters in current string
    string = normalize('NFKD', string).encode('ASCII', 'ignore').decode()

    # Remove every letter which are not alphanumeric
    result = re_sub(r'[^\w\d]+', ' ', string)

    # Replace space with a dash
    result = re_sub(r'\s+', '-', result.strip())

    return result.lower()


def token_hex(nbytes=None):
    """ Generate a token using hexadecimal characters

    This function is a compat one for hashlib.token_hex which is only available
    for python 3.6+

    Thanks to https://stackoverflow.com/a/35161595

    Parameters
    ----------
    nbytes : int, optional
        Set final string length

    Returns
    -------
    str
        Generated token
    """

    if nbytes is None:
        nbytes = 50

    return "%0x" % getrandbits(nbytes * 4)


def get_mimetype(path):
    """ Retrieve the mimetype from a specific filepath

    Parameters
    ----------
    path : django.core.files.uploadedfile.InMemoryUploadedFile
        Filepath

    Returns
    -------
    str
        File mimetype
    """

    return magic.from_buffer(path.read(), mime=True)


def is_allowed(mimetype, avatar=False):
    """ Check if a specific mimetype is allowed

    Parameters
    ----------
    mimetype : str
        Mimetype value
    avatar : bool
        Check in avatar extensions (Default: False)

    Returns
    -------
    bool
        Allowed status
    """

    extensions = settings.ALLOWED_EXTENSIONS

    if avatar:
        extensions = settings.ALLOWED_AVATAR_EXTENSIONS

    return mimetype in extensions.keys()
