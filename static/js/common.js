/* --------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * -------------------------------------------------------------------------- */

/**
 *  Function which alternate visible status of a sidebar menu
 *
 *  Parameters
 *  ----------
 *  name : str
 *      Menu widget identifier name
 */
function switch_status(name) {
    var widget = document.getElementById(name);

    if(widget != null) {
        var status = widget.style.display;

        // Retrieve style from CSS if no value is available
        if(status.length == 0) {
            status = getComputedStyle(widget, null).display;
        }

        switch(status) {
            case "block":
                widget.style.display = "none";
                break;

            case "none":
                widget.style.display = "block";
                break;
        }
    }
}
